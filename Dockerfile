FROM centos:7

RUN yum install -y https://repos.fedorapeople.org/repos/openstack/openstack-rocky/rdo-release-rocky-0.noarch.rpm \
    && yum install -y \
            python-openstackclient \
            python-neutronclient \
            python-heatclient \
    && yum clean all \
    && rm -rf /var/cache/yum

RUN mkdir /etc/openstack/
ADD entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["/bin/bash"]
