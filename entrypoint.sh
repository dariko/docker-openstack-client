#!/bin/bash
set -e
set -u

if [[ -v CLOUDS_YAML ]]; then
    cat > /etc/openstack/clouds.yaml <<<$CLOUDS_YAML
fi

exec "$@"
